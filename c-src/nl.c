#include <netlink/socket.h>
#include <netlink/netlink.h>
#include <netlink/msg.h>
#include <netlink/genl/genl.h>
#include <netlink/genl/ctrl.h>
#include <stdbool.h>

volatile bool done = false;
int nl_cb_valid(struct nl_msg *msg, void *_data) {
	struct genlmsghdr *gnlh = nlmsg_data(nlmsg_hdr(msg));
	printf("cmd: %ld\n", gnlh->cmd);
	FILE *fptr = stdout;
	nl_msg_dump(msg, fptr);
//	printf("A %X\n", nlmsg_data(nlmsg_hdr(ms));
	done = true;
	return NL_STOP;
}

int main() {
	struct nl_sock *sock = nl_socket_alloc();
	if (sock == NULL) {
		printf("null pointer\n");
		return 1;
	}
	int res;
	if ((res = nl_connect(sock, 16)) < 0) {
		printf("error %d\n", res);
		return 1;
	}
        nl_socket_set_buffer_size(sock, 8192, 8192);
	int err = 1;
        setsockopt(nl_socket_get_fd(sock), SOL_NETLINK, NETLINK_EXT_ACK, &err, sizeof(err));

	int family = genl_ctrl_resolve(sock, "nl80211");
	if (family < 0) {
		printf("res error %d\n", family);
		return 1;
	}
	struct nl_msg *msg = nlmsg_alloc();
	if (msg == NULL) {
		printf("null pointer\n");
		return 1;
	}

	printf("family: %d\n", family);
	genlmsg_put(msg, 0, 0, family,0,0, 5, 0);
	nla_put_u32(msg, 3, 3);
	done = false;
	res = nl_send_auto(sock, msg);
	if (res < 0) {
		printf("send error %d\n", res);
		return 1;
	}
	nl_socket_modify_cb(sock, 0, 3, nl_cb_valid, NULL);
	res = nl_recvmsgs_default(sock);
	if (res < 0) {
		printf("rx error %d\n", res);
		return 1;
	}
	while(!done) {sleep(0.100);};
	nl_socket_free(sock);
	nl_close(sock);
	return 0;
}
