
mod nl {

use std::io::Error;
use libc::{setsockopt, c_void};

#[repr(C)]
pub struct nl_sock {
    _unused: [u8; 0],
}

#[repr(C)]
pub struct nl_cb {
    _unused: [u8; 0],
}
#[repr(C)]
pub struct nl_err {
    _unused: [u8; 0],
}


mod message {
#[repr(C)]
    #[derive(Debug)]
    pub struct nl_msg {
        pub _private: [u8; 0],
    }
#[repr(C)]
    pub struct nlmsghdr {
        _unused: [u8; 0],
    }
}

type Cbwrap = extern "C" fn(msg: *mut message::nl_msg, arg: *const u8) -> i32;
#[link(name="nl-3")]
extern "C" {
    fn nl_socket_alloc() -> *const nl_sock;
    fn nl_socket_free(socket: *const nl_sock);
    fn nl_socket_get_fd(socket: *const nl_sock) -> i32;
    fn nl_socket_set_buffer_size(socket: *const nl_sock, rxbuf: isize, txbuf: isize) -> i32;
    fn nl_socket_modify_cb(socket: *const nl_sock, ctype: i32, kind: i32, func: Cbwrap, args: *const u8);
    fn nl_cb_err(cb: *const nl_cb, kind: i32, func: extern "C" fn(sock: *const nl_sock, err: *const nl_err, arg: *const c_void)->i32, args: *const c_void);
//    fn nl_socket_set_cb(socket: *const nl_sock, cb: *const ::callback::nl_cb);
//    fn nl_socket_get_cb(socket: *const nl_sock) -> ::callback::nl_cb;

    fn nl_socket_set_local_port(socket: *const nl_sock, port: u32);
    fn nl_socket_get_local_port(socket: *const nl_sock) -> u32;

    fn nl_connect(socket: *const nl_sock, protocol: isize) -> isize;
    fn nl_close(socket: *const nl_sock);

    fn nl_send_simple(socket: *const nl_sock, msg_type: isize, flags: isize, buf: *const c_void, size: isize) -> i32;
    fn nl_sendto(socket: *const nl_sock, buf: *const c_void, size: isize) -> i32;
    fn nl_send(socket: *const nl_sock, msg: *const message::nl_msg) -> i32;
    fn nl_send_auto(socket: *const nl_sock, msg: *const message::nl_msg) -> i32;
    fn nl_sendmsg(socket: *const nl_sock, msg: *const message::nl_msg, hdr: *const message::nlmsghdr) -> i32;
    fn nl_recvmsgs_default(socket: *const nl_sock) -> i32;
    fn nl_cb_alloc(kind: i32) -> *const nl_cb;
}

#[link(name="nl-genl-3")]
extern "C" {
    fn genl_ctrl_resolve(socket: *const nl_sock, name: *const i8) -> libc::c_int;
    fn nlmsg_alloc() -> *const message::nl_msg;
    fn genlmsg_put(msg: *const message::nl_msg, port: u32, seq: u32, family: i32, hdrlen: i32, flags: i32, cmd: u8, version: u8);
    fn nla_put_u32(msg: *const message::nl_msg, attr: i32, value: i32) -> i32;
    fn nl_data_get_size(msg: *const message::nl_msg) -> isize;
    fn nl_data_get(msg: *const message::nl_msg) -> *const u8;
}

pub const NETLINK_EXT_ACK : i32 = 11;
pub enum nl80211_cmd {
    GetInterface = 5,
    GetStation = 19,
}
pub enum nl80211_attr {
    IfIndex = 3, 
}

pub enum Type {
    Valid = 0,
    Finish = 1,
    Overrun = 2,
    Skipped = 3,
    Ack = 4,
    MsgIn = 5,
    MsgOut = 6,
    iInvalid = 7,
    SeqCheck = 8,
    SendAck = 9,
    DumpIntr = 10,
    __End = 11,
}
pub struct Socket {
    sock: *const nl_sock,
}

pub enum Protocol {
    Generic = 16 as isize,
}

extern fn nl_cb_valid(msg: *mut message::nl_msg, _foo: *const u8) -> i32 {
    println!("fp {:?}", _foo);
    let data: Option<&[u8]> = unsafe {
        //libc::printf(_foo as *const i8);
        let data = nl_data_get(msg);
        let len = nl_data_get_size(msg);
        if len < 0 || data == std::ptr::null() {
            println!("length {}", len);
            None
        } else {
            Some(std::slice::from_raw_parts(data as *const u8, len as usize))
        }
    };
    
    let data = data.unwrap_or(&[]);
    println!("length: {}", data.len());
   // println!("valid called {:?}", data);
    for (i, byte) in data.iter().enumerate() {
        if (i % 16) == 0 {
            println!("");
        }
        print!("{:02X}:", byte);
    }
    0
}
extern fn nl_cb_finish(msg: *const message::nl_msg, data: *const u8) -> i32 {
    println!("finish called");
    0
}
extern fn nl_cb_error(msg: *const nl_sock, err: *const nl_err, data: *const c_void) -> i32 {
    println!("error called");
    0
}
impl Socket {
    pub fn new() -> Option<Socket> {
        unsafe {
            let sock = nl_socket_alloc();
            let sock = Self { sock };
            if sock.sock == std::ptr::null() {
                return None;
            }
            nl_socket_modify_cb(sock.sock, Type::Valid as i32, 3, nl_cb_valid, std::ptr::null());
            //let cb = nl_cb_alloc(3);
            //println!("{:?}", cb);
            //nl_cb_err(cb, 3, nl_cb_error, std::ptr::null());
            Some(sock)
        }
    }

    pub fn bitrate(&self, if_index: i32) -> Result<(), std::io::Error> {
        let family = self.generic_ctrl_resolve("nl80211")?;
        println!("family: {}", family);
        unsafe {
            let msg = nlmsg_alloc();
            genlmsg_put(msg, 0, 0, family, 0, 0, nl80211_cmd::GetInterface as u8, 0);
            nla_put_u32(msg, nl80211_attr::IfIndex as i32, if_index);
            let res = nl_send_auto(self.sock, msg);
            if res < 0 {
                return Err(Error::from_raw_os_error(res));
            }
            let res = nl_recvmsgs_default(self.sock); if res < 0 {
                return Err(Error::from_raw_os_error(res));
            }
            Ok(())
        }
    }

    pub fn connect(&self, proto: Protocol) -> Result<(), std::io::Error> {
        let sock = unsafe {
            nl_connect(self.sock, proto as isize)
        };
        if sock < 0 {
            return Err(Error::from_raw_os_error(sock as i32));
        }
        Ok(())
    }

    pub fn set_buffer_size(&self, tx: isize, rx: isize) {
        unsafe {
            nl_socket_set_buffer_size(self.sock, tx, rx);
        }
    }

    pub fn generic_ctrl_resolve(&self, name: &str) -> Result<i32, std::io::Error> {
        let name = std::ffi::CString::new(name)?;
        let id = unsafe {
            //let name = "nl80211\0".as_ptr() as *const i8;
            genl_ctrl_resolve(self.sock, name.as_ptr())
        };
        if id < 0 {
            println!("error code {}", id);
            return Err(std::io::Error::from_raw_os_error(id));
        }

        Ok(id)
    }

    pub fn as_raw_fd(&self) -> i32 {
        unsafe { nl_socket_get_fd(self.sock) }
    }

}

impl Drop for Socket {
    fn drop(&mut self) {
        unsafe { nl_socket_free(self.sock); println!("drop"); };
    }
}
}
use libc::{setsockopt, c_void};
fn main() {
    let nl = nl::Socket::new().unwrap();
    nl.connect(nl::Protocol::Generic).expect("connect failed");
    nl.set_buffer_size(8192, 8192);
    unsafe {
        let err: [i32; 1]= [1];
        setsockopt(nl.as_raw_fd(), libc::SOL_NETLINK, nl::NETLINK_EXT_ACK, (&err).as_ptr() as *const c_void, 4);
    }

    nl.bitrate(3);
    loop {}
}
